from Producto import Producto

class Electrodomestico(Producto):
    def __init__(self, peso, codigo, marca, descripcion, garantia, color, tamanio, alimentacion, potencia, modelo, precio, cantidad):
        super().__init__(peso, codigo, marca, descripcion, precio, cantidad)
        self.__garantia = garantia
        self.__color = color
        self.__tamanio = tamanio
        self.__alimentacion = alimentacion
        self.__potencia = potencia
        self.__modelo = modelo
    
    def setGarantia(self, garantia):
        self.__garantia = garantia

    def getGarantia(self):
        return self.__garantia
    
    def setColor(self, color):
        self.__color = color
    
    def getColor(self):
        return self.__color
    
    def setTamanio(self, tamanio):
        self.__tamanio = tamanio

    def getTamanio(self):
        return self.__tamanio

    def setAlimentacion(self, alimentacion):
        self.__alimentacion = alimentacion

    def getAlimentacion(self):
        return self.__alimentacion
    
    def setPotencia(self, potencia):
        self.__potencia = potencia
    
    def getPotencia(self):
        return self.__potencia
    
    def setModelo(self, modelo):
        self.__modelo = modelo
    
    def getModelo(self):
        return self.__modelo