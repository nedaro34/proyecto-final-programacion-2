from Alimento import *
from Carrito import *
from Cliente import *
from Compra import *
from Electrodomestico import *
from Mercado import *
from Producto import *
from os import system
import copy

def borrarPantalla():
    system("cls")


def detenerPantalla():
    system("pause")


def multiplicarUnidades(producto, cliente):
    while True:
        try:
            borrarPantalla()
            print(f"Stock diponible de {producto.getDescripcion()} {producto.getMarca()}: {producto.getCantidad()}")
            cantidad = input(f"Ingrese la cantidad que desea agregar al carrito, 0 para cancelar: ")
            if not cantidad.isdigit():
                raise ValueError("\nError, solo se permite el ingreso de números positivos")
            cantidad = int(cantidad)
            if cantidad > producto.getCantidad():
                raise ValueError("\nError, no se cuenta con stock suficiente")
            break
        except ValueError as err:
            print(err)
            detenerPantalla()

    try:
        if cantidad:
            producto_carrito = copy.copy(producto)
            producto_carrito.setCantidad(cantidad)
            cliente.getCarrito().agregar(producto_carrito)
    except copy.error:
        print("No se pudo seleccionar esa cantidad")
        


def filtrarCompra(mercado, cliente, tipo = True):
    productos_filtrados = []
    while True:
        try:
            borrarPantalla()
            for producto in mercado.getProductos():
                if isinstance(producto, Alimento if tipo else Electrodomestico):
                    productos_filtrados.append(producto)
            for i in range(len(productos_filtrados)):
                print(f"{i+1} - {productos_filtrados[i].getDescripcion()} {productos_filtrados[i].getMarca()}")
            seleccion = input(f"\n\nSeleccione un producto para comprar, 0 para salir: ")
            if not seleccion.isdigit():
                raise ValueError("\nError, solo se permite el ingreso de números")
            seleccion = int(seleccion)
            if seleccion > len(productos_filtrados) or seleccion < 0:
                raise ValueError("\nError, valor no válido")
            break
        except ValueError as err:
            productos_filtrados.clear()
            print(err)
            detenerPantalla()

    try:
        if seleccion:
            producto = productos_filtrados[seleccion-1]
            multiplicarUnidades(producto, cliente)
    except IndexError:
        print("Error, no se pudo acceder a la lista")


def agregarProductos(mercado, cliente):
    while True:
        borrarPantalla()
        print("Seleccione el tipo de producto que desea comprar:\n\t1 - Alimentos\n\t2 - Electrodomésticos\n\t3 - Cancelar")
        seleccion = int(input("\nSelección: "))
        try:
            match seleccion:
                case 1: filtrarCompra(mercado, cliente)
                case 2: filtrarCompra(mercado, cliente, False)
                case 3: break
                case _: raise ValueError
        except ValueError:
            print("\nError, ingresó una opción no válida")
            detenerPantalla()

def cambiarCantidad(mercado, producto):
    while True:
        try:
            borrarPantalla()
            print("Stock disponible:", mercado.buscarProducto(producto.getCodigo()).getCantidad())
            cantidad = input("Ingrese nueva cantidad: ")
            if not cantidad.isdigit() or int(cantidad) < 1:
                raise ValueError("Error, solo se pueden ingresar números positivos")
            cantidad = int(cantidad)
            if cantidad < 1 or cantidad > mercado.buscarProducto(producto.getCodigo()).getCantidad():
                raise ValueError("Error, no se pueden ingresar cantidades mayores al stock disponible")
            producto.setCantidad(cantidad)
            break
        except ValueError as err:
            print(err)
            detenerPantalla()
    
    print("Cantidad modificada con éxito")
    detenerPantalla()

def verDetalles(mercado, producto):
    while True:
        try:
            borrarPantalla()
            print(f"{producto.getDescripcion()} {producto.getMarca()}")
            print(f"Precio: ${producto.getPrecio()} x {producto.getCantidad()} Total: ${producto.getPrecio() * producto.getCantidad()}")
            print("Peso:", producto.getPeso(), "Código:", producto.getCodigo())
            if isinstance(producto, Alimento):
                print(f"Fecha de vencimiento: {producto.getFechaVencimiento()}")
            else:
                print("Garantía:", producto.getGarantia(), "Color:", producto.getColor())
                print("Tamaño:", producto.getTamanio(), "Alimentación:", producto.getAlimentacion())
                print("Potencia:", producto.getPotencia(), "Modelo:", producto.getModelo())


            seleccion = input("\nDesea cambiar la cantidad de unidades de este producto? s/n: ").lower()
            if seleccion != "s" and seleccion != "n":
                raise ValueError("\nError, opción no válida")
            break
        except ValueError as err:
            print(err)
            detenerPantalla()
        
    if seleccion == "s":
        cambiarCantidad(mercado, producto)

def verCarrito(mercado, cliente):
    while True:
        try:
            borrarPantalla()
            if len(cliente.getCarrito().getProductos()) == 0:
                print("El carrito se encuentra vacío")
            else:
                for i in range(len(cliente.getCarrito().getProductos())):
                    print(f"{i+1} - {cliente.getCarrito().getProductos()[i].getDescripcion()} {cliente.getCarrito().getProductos()[i].getMarca()} ${cliente.getCarrito().getProductos()[i].getPrecio()} x {cliente.getCarrito().getProductos()[i].getCantidad()}")
                
                seleccion = input("\nIngrese un producto para ver detalles o cambiar su cantidad, 0 para salir: ")
                if not seleccion.isdigit():
                    raise ValueError("\nError, solo se permite el ingreso de números")
                seleccion = int(seleccion)
                if seleccion > len(cliente.getCarrito().getProductos()) or seleccion < 0:
                    raise ValueError("\nError, valor no válido")
            break
        except ValueError as err:
            print(err)
            detenerPantalla()
    
    if seleccion:
        producto = cliente.getCarrito().getProductos()[seleccion-1]
        verDetalles(mercado, producto)


def comprarCarrito(mercado, cliente):
    while True:
        try:
            borrarPantalla()
            if len(cliente.getCarrito().getProductos()) == 0:
                print("El carrito se encuentra vacío")
                seleccion = "n"
                break
            else:
                print("Desea comprar los productos del carrito?\n")
                
                for i in range(len(cliente.getCarrito().getProductos())):
                    print(f"{i+1} - {cliente.getCarrito().getProductos()[i].getDescripcion()} {cliente.getCarrito().getProductos()[i].getMarca()} ${cliente.getCarrito().getProductos()[i].getPrecio()} x {cliente.getCarrito().getProductos()[i].getCantidad()}")
                
                seleccion = input("\nSelección s/n: ").lower()
                if seleccion != "s" and seleccion != "n":
                    raise ValueError("\nError, opción no válida")
                break
        except ValueError as err:
            print(err)

    if seleccion == "s":
        productos = []
        for producto in cliente.getCarrito().getProductos():
            mercado.disminuirStock(producto.getCodigo(), producto.getCantidad())
            productos.append(producto)
        compra = Compra(mercado.cantidadCompras() + 1, "6/7/2023", productos, cliente)
        cliente.agregarCompra(compra)
        cliente.getCarrito().vaciar()
        print("\nProductos comprados con éxito")
        detenerPantalla()


def verCompras(cliente):
    borrarPantalla()
    for compra in cliente.getCompras():
        print(f"Número de compra: {compra.getNumeroCompra()}")
        for producto in compra.getProductos():
            print(f"{producto.getDescripcion()} {producto.getMarca()} ${producto.getPrecio()} x {producto.getCantidad()}")
    detenerPantalla()


def inicio():
    mercado = Mercado("Carrefour", "CENTROS COMERCIALES CARREFOUR, S.A.")
    cliente = Cliente("Nelson", "Daniel", "40724681", "3834977874", Carrito())
    mercado.agregarCliente(cliente)

    producto = Alimento("500gr", 100001, "Terrabusi", "Fideos tallarines", "20/11/2023", 620, 5)
    mercado.agregarProducto(producto)
    producto = Alimento("500gr", 100002, "Carrefour", "Mermelada", "21/12/2024", 450, 9)
    mercado.agregarProducto(producto)

    producto = Electrodomestico("10Kg", 200001, "Coinor", "lava ropas automático", "1 año", "gris", "120x120x100 cm", "220v", "200w", "st21a", 60000, 5)
    mercado.agregarProducto(producto)

    while True:
        
        while True:
            try:
                borrarPantalla()
                print("Seleccione una opción:\n\t\t1 - Agregar productos al carrito\n\t\t2 - Ver carrito\n\t\t3 - Comprar carrito\n\t\t4 - Ver compras\n\t\t9 - Salir")
                seleccion = input("Selección: ")
                if not seleccion.isdigit():
                    raise ValueError("\nError, solo se permite el ingreso de números")
                seleccion = int(seleccion)
                break
            except ValueError as err:
                print(err)
                detenerPantalla()

        try:
            match seleccion:
                case 1: agregarProductos(mercado, cliente)
                case 2: verCarrito(mercado, cliente)
                case 3: comprarCarrito(mercado, cliente)
                case 4: verCompras(cliente)
                case 9: break
                case _: raise ValueError
        except ValueError:
            print("\nError, ingresó una opción no válida")
            detenerPantalla()

    borrarPantalla()

inicio()