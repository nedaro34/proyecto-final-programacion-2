class Cliente:
    def __init__(self, nombre, apellido, documento, telefono, carrito):
        self.__nombre = nombre
        self.__apellido = apellido
        self.__documento = documento
        self.__telefono = telefono
        self.__compras = []
        self.__carrito = carrito
    
    def setNombre(self, nombre):
        self.__nombre = nombre
    
    def getNombre(self):
        return self.__nombre
    
    def setApellido(self, apellido):
        self.__apellido = apellido
    
    def getApellido(self):
        return self.__apellido
    
    def setDocumento(self, documento):
        self.__documento = documento

    def getDocumento(self):
        return self.__documento
    
    def setTelefono(self, telefono):
        self.__telefono = telefono
    
    def getTelefono(self):
        return self.__telefono
    
    def setCompras(self, compras):
        self.__compras = compras
    
    def getCompras(self):
        return self.__compras
    
    def setCarrito(self, carrito):
        self.__carrito = carrito
    
    def getCarrito(self):
        return self.__carrito
    
    def agregarCompra(self, compra):
        self.__compras.append(compra)
    
    def eliminarCompra(self, numero_compra):
        encontrado = None

        for compra in self.__compras:
            if compra.getNumeroCompra() == numero_compra:
                encontrado = compra
                break
        
        if encontrado:
            self.__compras.remove(encontrado)
    
    def cantidadCompras(self):
        return len(self.__compras)