class Producto:
    def __init__(self, peso, codigo, marca, descripcion, precio, cantidad):
        self.__peso = peso
        self.__codigo = codigo
        self.__marca = marca
        self.__descripcion = descripcion
        self.__precio = precio
        self.__disponibilidad = True
        self.__cantidad = cantidad
    
    def setPeso(self, peso):
        self.__peso = peso

    def getPeso(self):
        return self.__peso
    
    def setCodigo(self, codigo):
        self.__codigo = codigo
    
    def getCodigo(self):
        return self.__codigo
    
    def setMarca(self, marca):
        self.__marca = marca
    
    def getMarca(self):
        return self.__marca
    
    def setDescripcion(self, descripcion):
        self.__descripcion = descripcion
    
    def getDescripcion(self):
        return self.__descripcion
    
    def setPrecio(self, precio):
        self.__precio = precio
    
    def getPrecio(self):
        return self.__precio
    
    def setDisponibilidad(self):
        self.__disponibilidad = True
    
    def getDisponibilidad(self):
        return self.__disponibilidad

    def darDeBaja(self):
        self.__disponibilidad = False
    
    def setCantidad(self, cantidad):
        self.__cantidad = cantidad
    
    def getCantidad(self):
        return self.__cantidad
    
    def agregarCantidad(self, cantidad):
        self.__cantidad += cantidad