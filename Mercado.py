class Mercado:
    def __init__(self, nombre, razon_social):
        self.__nombre = nombre
        self.__razon_social = razon_social
        self.__clientes = []
        self.__productos = []
    
    def setNombre(self, nombre):
        self.__nombre = nombre
    
    def getNombre(self):
        return self.__nombre
    
    def setRazonSocial(self, razon_social):
        self.__razon_social = razon_social
    
    def getRazonSocial(self):
        return self.__razon_social
    
    def setClientes(self, clientes):
        self.__clientes = clientes
    
    def getClientes(self):
        return self.__clientes
    
    def setProductos(self, productos):
        self.__productos = productos
    
    def getProductos(self):
        return self.__productos
    
    def agregarCliente(self, cliente):
        self.__clientes.append(cliente)
    
    def eliminarCliente(self, documento):
        encontrado = None

        for cliente in self.__clientes:
            if cliente.getDocumento() == documento:
                encontrado = cliente
                break
        
        if encontrado:
            self.__clientes.remove(encontrado)
    
    def agregarProducto(self, producto):
        self.__productos.append(producto)
    
    def buscarProducto(self, codigo):
        encontrado = None

        for producto in self.__productos:
            if producto.getCodigo() == codigo:
                encontrado = producto
                break

        return encontrado
    
    def eliminarProducto(self, codigo):
        encontrado = None

        for producto in self.__productos:
            if producto.getCodigo() == codigo:
                encontrado = producto
                break
        
        self.__productos.remove(encontrado)
    
    def disminuirStock(self, codigo, cantidad):
        for producto in self.__productos:
            if producto.getCodigo() == codigo:
                producto.setCantidad(producto.getCantidad() - cantidad)
                break
    
    def cantidadCompras(self):
        cantidad = 0
        for cliente in self.__clientes:
            cantidad += cliente.cantidadCompras()
        return cantidad