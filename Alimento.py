from Producto import Producto

class Alimento(Producto):
    def __init__(self, peso, codigo, marca, descripcion, fecha_vencimiento, precio, cantidad):
        super().__init__(peso, codigo, marca, descripcion, precio, cantidad)
        self.__fecha_vencimiento = fecha_vencimiento
    
    def setFechaVencimiento(self, fecha_vencimiento):
        self.__fecha_vencimiento = fecha_vencimiento
    
    def getFechaVencimiento(self):
        return self.__fecha_vencimiento