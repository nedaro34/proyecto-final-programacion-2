from Alimento import *
from Carrito import *
from Cliente import *
from Compra import *
from Electrodomestico import *
from Mercado import *
from Producto import *
from os import system

def borrarPantalla():
    system("cls")

def detenerPantalla():
    system("pause")

def comprar(producto, cliente):
    while True:
        try:
            borrarPantalla()
            print(f"Stock diponible de {producto.getDescripcion()} {producto.getMarca()}: {producto.getCantidad()}")
            cantidad = input(f"Ingrese la cantidad que desea agregar al carrito, 0 para cancelar: ")
            if not cantidad.isdigit():
                raise ValueError("\nError, solo se permite el ingreso de números positivos")
            cantidad = int(cantidad)
            if cantidad > producto.getCantidad():
                raise ValueError("\nError, no se cuenta con stock suficiente")
            break
        except ValueError as err:
            print(err)
            detenerPantalla()

    if cantidad:
        carrito = cliente.getCarrito()
        if isinstance(producto, Alimento):
            alimento = Alimento(producto.getPeso(), producto.getCodigo(), producto.getMarca(), producto.getDescripcion(), producto.getFechaVencimiento(), producto.getPrecio(), cantidad)
            carrito.agregar(alimento)
        else:
            electrodomestico = Electrodomestico(producto.getPeso(), producto.getCodigo(), producto.getMarca(), producto.getDescripcion(), producto.getGarantia(), producto.getColor(), producto.getTamanio(), producto.getAlimentacion(), producto.getPotencia, producto.getModelo() , producto.getPrecio(), cantidad)
            carrito.agregar(electrodomestico)

def comprarAlimentos(mercado, cliente):
    alimentos = []
    while True:
        try:
            borrarPantalla()
            for producto in mercado.getProductos():
                if isinstance(producto, Alimento):
                    alimentos.append(producto)
            for i in range(len(alimentos)):
                print(f"{i+1} - {alimentos[i].getDescripcion()} {alimentos[i].getMarca()}")
            seleccion = input(f"\n\nSeleccione un alimento para comprar, 0 para salir: ")
            if not seleccion.isdigit():
                raise ValueError("\nError, solo se permite el ingreso de números")
            seleccion = int(seleccion)
            if seleccion > len(alimentos) or seleccion < 0:
                raise ValueError("\nError, valor no válido")
            break
        except ValueError as err:
            alimentos.clear()
            print(err)
            detenerPantalla()
    
    try:
        if seleccion:
            alimento = alimentos[seleccion-1]
            comprar(alimento, cliente)
    except IndexError:
        print("Error, no se pudo acceder a la lista")


def comprarElectrodomesticos(mercado, cliente):
    electrodomesticos = []
    while True:
        try:
            borrarPantalla()
            for producto in mercado.getProductos():
                if isinstance(producto, Electrodomestico):
                    electrodomesticos.append(producto)

            for i in range(len(electrodomesticos)):
                print(f"{i+1} - {electrodomesticos[i].getDescripcion()} {electrodomesticos[i].getMarca()}")

            seleccion = input(f"\n\nSeleccione un electrodoméstico para comprar, 0 para salir: ")
            if not seleccion.isdigit():
                raise ValueError("\nError, solo se permite el ingreso de números")
            seleccion = int(seleccion)
            if seleccion > len(electrodomesticos) or seleccion < 0:
                raise ValueError("\nError, valor no válido")
            break
        except ValueError as err:
            electrodomesticos.clear()
            print(err)
            detenerPantalla()

    try:
        if seleccion:
            electrodomestico = electrodomesticos[seleccion-1]
            comprar(electrodomestico, cliente)
    except IndexError:
        print("Error, no se pudo acceder a la lista")


def agregarProductos(mercado, cliente):
    while True:
        borrarPantalla()
        print("Seleccione el tipo de producto que desea comprar:\n\t1 - Alimentos\n\t2 - Electrodomésticos\n\t3 - Cancelar")
        seleccion = int(input("\nSelección: "))
        try:
            match seleccion:
                case 1: comprarAlimentos(mercado, cliente)
                case 2: comprarElectrodomesticos(mercado, cliente)
                case 3: break
                case _: raise ValueError
        except ValueError:
            print("\nError, ingresó una opción no válida")
            detenerPantalla()
    

def verCarrito(cliente):
    borrarPantalla()
    for i in range(len(cliente.getCarrito().getProductos())):
        print(f"{i+1} - {cliente.getCarrito().getProductos()[i].getDescripcion()} {cliente.getCarrito().getProductos()[i].getMarca()} ${cliente.getCarrito().getProductos()[i].getPrecio()} x {cliente.getCarrito().getProductos()[i].getCantidad()}")
    
    detenerPantalla()


def comprarCarrito(mercado, cliente):
    borrarPantalla()
    print("Desea comprar los productos del carrito?\n")
    
    for i in range(len(cliente.getCarrito().getProductos())):
        print(f"{i+1} - {cliente.getCarrito().getProductos()[i].getDescripcion()} {cliente.getCarrito().getProductos()[i].getMarca()} ${cliente.getCarrito().getProductos()[i].getPrecio()} x {cliente.getCarrito().getProductos()[i].getCantidad()}")
    
    seleccion = True if input("Selección s/n: ").lower() == "s" else False

    if seleccion:
        productos = []
        for producto in cliente.getCarrito().getProductos():
            mercado.disminuirStock(producto.getCodigo(), producto.getCantidad())
            productos.append(producto)
        compra = Compra(mercado.cantidadCompras() + 1, "6/7/2023", productos, cliente)
        cliente.agregarCompra(compra)
        cliente.getCarrito().vaciar()
    detenerPantalla()

def verCompras(cliente):
    borrarPantalla()
    for compra in cliente.getCompras():
        print(f"Número de compra: {compra.getNumeroCompra()}")
        for producto in compra.getProductos():
            print(f"{producto.getDescripcion()} {producto.getMarca()} ${producto.getPrecio()} x {producto.getCantidad()}")
    detenerPantalla()

def inicio():
    mercado = Mercado("Carrefour", "CENTROS COMERCIALES CARREFOUR, S.A.")
    cliente = Cliente("Nelson", "Daniel", "40724681", "3834977874", Carrito())
    mercado.agregarCliente(cliente)

    producto = Alimento("500gr", 100001, "Terrabusi", "Fideos tallarines", "20/11/2023", 620, 5)
    mercado.agregarProducto(producto)

    producto = Electrodomestico("10Kg", 200001, "Coinor", "lava ropas automático", "1 año", "gris", "120x120x100 cm", "220v", "200w", "st21a", 60000, 5)
    mercado.agregarProducto(producto)

    while True:
        borrarPantalla()
        print("Seleccione una opción:\n\t\t1 - Agregar productos al carrito\n\t\t2 - Ver carrito\n\t\t3 - Comprar carrito\n\t\t4 - Ver compras\n\t\t9 - Salir")
        
        seleccion = int(input("Selección: "))

        match seleccion:
            case 1: agregarProductos(mercado, cliente)
            case 2: verCarrito(cliente)
            case 3: comprarCarrito(mercado, cliente)
            case 4: verCompras(cliente)
            case 9: break

    borrarPantalla()

inicio()