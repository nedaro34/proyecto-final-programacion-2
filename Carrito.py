class Carrito:
    def __init__(self):
        self.__productos = []
    
    def getProductos(self):
        return self.__productos

    def agregar(self,producto):
        self.__productos.append(producto)
    
    def eliminar(self, codigo):
        encontrado = None

        for producto in self.__productos:
            if producto.getCodigo() == codigo:
                encontrado = producto
                break
        
        if encontrado:
            self.__productos.remove(encontrado)
    
    def vaciar(self):
        self.__productos.clear()
    
    def contarProductos(self):
        return len(self.__productos)
    
    def montoTotal(self):
        monto = 0
        
        for producto in self.__productos:
            monto += producto.getPrecio()
        
        return monto